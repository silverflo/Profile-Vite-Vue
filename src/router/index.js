// Import the necessary functions and components from Vue Router and your views
import { createWebHistory, createRouter } from "vue-router";
import Home from "@/views/home.vue";
import Form from "@/views/form.vue";
import Login from "@/views/login.vue"
import ReviewList from "@/views/reviewList.vue";

// Define routes using an array of objects
const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/feedback",
    name: "Form",
    component: Form,
  },
  {
    path: "/reviews",
    name: "Reviews",
    component: ReviewList,
  },
  {
    path: "/auth",
    name: "Login",
    component: Login,
  },
];

// Create a new router instance using the createRouter function
const router = createRouter({
  history: createWebHistory(),
  routes,
});

// Export the router instance as the default export of this module
export default router;
